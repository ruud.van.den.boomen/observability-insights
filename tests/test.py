import pytest
from monitored_endpoints import format_endpoint


@pytest.mark.parametrize(
    "input_endpoint, expected_output",
    [
        ("api/v2/parcels/_p_pk_0-9_/", "api/v2/parcels/<pk>/"),
        ("api/v2/integrations/_p_pk_/._/", "api/v2/integrations/<pk>/"),
        (
            "api/v2/parcels/_p_pk_0-9_/documents/_p_type_slug_-_w_/",
            "api/v2/parcels/<pk>/documents/<type_slug>/",
        ),
        (
            "api/v2/brand/_slug:domain_/return-portal/outgoing/",
            "api/v2/brand/<slug:domain>/return-portal/outgoing/",
        ),
    ],
)
def test_format_endpoint(input_endpoint, expected_output):
    result = format_endpoint(input_endpoint)
    assert result == expected_output
