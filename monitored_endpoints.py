import csv
import re

# Path to Terraform monitor configuration
datadog_configuration_path = (
    "/home/ruud/Projects/datadog/generated/datadog/monitor/combined_ouput.tf"
)
# Run `./python/manage.py show_urls | endpoints.tsv` in SCP to generate this file.
scp_endpoints_path = "/home/ruud/Projects/datadog/endpoints.tsv"

# Regex to find Datadog monitor queries(that use trace.django.request) in the specified terraform file.
ENDPOINT_REGEX = re.compile(
    r'query\s*=\s*\"\w*\(\w*\):(\w*):trace\.django\.request{.*resource_name:(post|get|put|patch|delete)_(.*?)(?:,.*)?}\s>\s(\d+(?:\.\d+)?)\"'
)
# In Django, we often use regular expression pattern for URL patterns.
# Datadog converts the special characters into underscores.
# This regex is used to capture those situations so we can substitute them.
PYTHON_PATH_REGEX = re.compile(r"(?<=/)(?:_p_|_)([a-z]{2,}(?:(?:_|:)[a-z]{2,})*)_(?:[0-9a-zA-Z-_]+_|)")


def format_endpoint(datadog_endpoint: str) -> str:
    """Format endpoints into easy readable string

    Example:
        api/v2/parcels/_p_pk_0-9_/ -> api/v2/parcels/<pk>/
        api/v2/integrations/_p_pk_/._/ -> get_api/v2/integrations/<pk>/
        api/v2/parcels/_p_pk_0-9_/documents/_p_type_slug_-_w_/ -> api/v2/parcels/<pk>/documents/<type_slug>/
        api/v2/brand/_slug:domain_/return-portal/outgoing/ -> api/v2/brand/<slug:domain>/return-portal/outgoing/
    """
    formatted_enpoint = PYTHON_PATH_REGEX.sub(r"<\1>", datadog_endpoint)
    formatted_enpoint = formatted_enpoint.replace("._/", "")
    formatted_enpoint = formatted_enpoint.replace("_/", "/")
    formatted_enpoint = formatted_enpoint.replace("/.", "")
    return formatted_enpoint


def get_available_endpoints(path):
    available_endpoints = set()
    non_relavant_endpoints = (
        # We are not interested in admin endpoints.
        "admin-ng",
        # Also not interested in endpoints with \.<format>/, since
        # Django automatically generates this variant of each endpoint
        "\.<format>/",
    )

    with open(path, "r") as endpoints_file:
        reader = csv.reader(endpoints_file, delimiter="\t", quotechar='"')

        for endpoint, location, reverse_url in reader:
            if all(value not in endpoint for value in non_relavant_endpoints):
                endpoint = endpoint[1:].replace("[/]", "/")
                if endpoint != "":
                    available_endpoints.add(endpoint)
    return available_endpoints


def get_monitored_endpoints(path):
    monitored_endpoints = set()

    with open(datadog_configuration_path, "r") as terraform_file:
        data_monitors_configuration = terraform_file.read()

        # Use regex to find the all matches in the Terraform file.
        match = re.findall(ENDPOINT_REGEX, data_monitors_configuration)
        match.sort(key=lambda tup: tup[2])

        print("Monitored SCP endpoints:")
        for metric, method, endpoint, threshold in match:
            formatted_enpoint = format_endpoint(endpoint)
            if formatted_enpoint in available_endpoints:
                print(f"\t- {method.upper():<10} {formatted_enpoint:<70} {metric:>10} < {threshold}")
            monitored_endpoints.add(formatted_enpoint)
    return monitored_endpoints


if __name__ == "__main__":
    available_endpoints = get_available_endpoints(scp_endpoints_path)
    monitored_endpoints = get_monitored_endpoints(datadog_configuration_path)

    # Endpoints that are available, but not monitored.
    missing_monitoring = sorted(available_endpoints.difference(monitored_endpoints))
    if missing_monitoring:
        print(f"\nSCP endpoints missing monitoring ({len(missing_monitoring)}):")
        print("\t- ", end= "")
        print("\n\t- ".join(missing_monitoring))

    # Endpoints that are monitored, but not found in the available endpoints are either formatted wrongly,
    # or belong to another project(e.g. Orders) which causes that we didn't find a match
    unkown_endpoints = sorted(monitored_endpoints.difference(available_endpoints))
    if unkown_endpoints:
        print("\nMonitored but not found in SCP endpoints:")
        print("\t- ", end= "")
        print("\n\t- ".join(unkown_endpoints))
